FROM debian:buster-slim

#update and install dependencies
RUN apt update && \
apt upgrade -y && \
apt install -y curl openssl libssl-dev pkg-config rustc cargo git mercurial capnproto clang sqlite3 libsqlite3-0 libsqlite3-dev python3 python3-lxml build-essential automake libtool autoconf make nettle-dev capnproto uuid-dev nano


ENV HOME=/root

#create folders
RUN mkdir $HOME/code && \
mkdir $HOME/code/common && \
mkdir $HOME/include && \
mkdir $HOME/lib && \
mkdir $HOME/bin && \
mkdir $HOME/share && \
mkdir -p $HOME/code/common/sequoia && \
mkdir -p $HOME/code/common/yml2 && \
mkdir -p $HOME/code/common/libetpan && \
mkdir -p $HOME/code/common/asn1c && \
mkdir -p $HOME/code/pEpEngine && \
mkdir -p $HOME/code/common/libpEpAdapter && \
mkdir -p $HOME/code/pEpPythonAdapter

#build and install sequoia
WORKDIR $HOME/code/common/sequoia
RUN git clone https://gitlab.com/sequoia-pgp/sequoia.git . && \
git checkout pep-engine && \
make build-release PYTHON=disable && \
make install PYTHON=disable PREFIX="$HOME"
RUN cargo clean

#build and install YML2
ENV PATH="$HOME/code/common/yml2:${PATH}"
WORKDIR $HOME/code/common/yml2
RUN apt install -y  python-setuptools && \
hg clone https://pep.foundation/dev/repos/yml2 . && \
make

#build and install libetpan
WORKDIR $HOME/code/common/libetpan
RUN git clone https://github.com/fdik/libetpan . && \
mkdir $HOME/code/common/libetpan/build && \
./autogen.sh --prefix=$HOME && \
make install

#build and install asn1c
WORKDIR $HOME/code/common/asn1c
RUN git clone git://github.com/vlm/asn1c.git . && \
git checkout tags/v0.9.28 -b pep-engine && \
autoreconf -iv && \
./configure --prefix="$HOME" && \
make install

#build pEp Engine
WORKDIR $HOME/code/pEpEngine
RUN hg clone https://pep.foundation/dev/repos/pEpEngine/ .
COPY local.conf $HOME/code/pEpEngine
RUN make && \ 
make install && \
make db dbinstall

#build pEp Adapter
WORKDIR $HOME/code/common/libpEpAdapter
RUN hg clone https://pep.foundation/dev/repos/libpEpAdapter/ . && \
make && \
make install PREFIX="$HOME"

#build Python Adapter
RUN apt install -y python3-setuptools libboost-python-dev libboost-locale1.67-dev python3-pip && \
pip3 install wheel
WORKDIR $HOME/code/pEpPythonAdapter
RUN hg clone https://pep.foundation/dev/repos/pEpPythonAdapter/ . && \
make

#set environment variables for libraries and python
RUN echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/lib' >> $HOME/.bashrc && \
echo 'export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$HOME/lib' >> $HOME/.bashrc && \
echo 'export PYTHONPATH=$PYTHONPATH:$HOME/code/pEpPythonAdapter/build/lib.linux-x86_64-3.7' >> $HOME/.bashrc && \
echo 'export PYTHONPATH=$PYTHONPATH:$HOME/code/pEpPythonAdapter/build/lib.macosx-10.9-x86_64-3.8' >> $HOME/.bashrc && \
echo 'export PYTHONPATH=$PYTHONPATH:$HOME/code/pEpPythonAdapter/src' >> $HOME/.bashrc 

WORKDIR /

ENTRYPOINT /bin/bash



